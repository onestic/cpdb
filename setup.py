from setuptools import setup

setup(
    name='cpdb',
    version='0.1.1',
    py_modules=['cpdb'],
    install_requires=[
        'Click',
        'ansible>=1.8',
        'MySQL-python'
    ],
    entry_points='''
        [console_scripts]
        cpdb=cpdb:cli
    '''
)
