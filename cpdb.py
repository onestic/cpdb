#!/usr/bin/env python
# -*- coding: utf8 -*-
import os
import click
import errno
import subprocess
import sys
import logging
import ansible.inventory
import ansible.runner


class Remote(object):
    """Show ansible output from inside a script"""
    def __init__(self, results, priority=2):
        self.results = results
        self.priority = priority

    def show(self):
        for (hostname, result) in self.results['contacted'].items():
            if 'failed' not in result:
                logging.info("OK: %s" % hostname)
            if 'failed' in result:
                if (self.priority > 1):
                    logging.critical("KO: %s\n%s" % (hostname, result['msg']))
                    sys.exit(1)
                else:
                    logging.error("KO: %s\n%s" % (hostname, result['msg']))
        for (hostname, result) in self.results['dark'].items():
            # if priority is bigger than 1 it stops (default: 2)
            if (self.priority > 1):
                logging.critical("%s is DOWN: %s" % (hostname, result['msg']))
                sys.exit(1)
            else:
                logging.error("%s is DOWN: %s" % (hostname, result['msg']))


class Cpdb(object):
    """Manage all the functions related with the dump"""
    def __init__(self, db, host="localhost", user="root", loglevel="info"):
        self.db = db
        self.host = host
        self.user = user
        self.dump_path = ''

        numeric_level = getattr(logging, loglevel.upper(), None)
        if not isinstance(numeric_level, int):
            raise ValueError('Invalid log level: %s' % loglevel)
        logging.basicConfig(level=numeric_level)

        if self.host != "localhost":
            hosts = [self.host]
            self.inventory = ansible.inventory.Inventory(hosts)

    def get(self):
        self.dump_path = "/tmp/%s.sql.gz" % self.db
        logging.info("Dumping database %s in %s to %s" % (self.db, self.host, self.dump_path))
        if self.host == "localhost":
            # First be sure the file doesn't exists
            self._local_remove(self.dump_path)
            retcode = subprocess.call(
                "mysqldump --single-transaction %s | gzip > %s" %
                (self.db, self.dump_path), shell=True
            )
            if retcode != 0:
                logging.critical("KO execution failed:", retcode)
                sys.exit(1)
            else:
                logging.info("OK: %s" % self.host)
        else:
            self._check_mysqldb()
            dumpremote = self._exec(
                'mysql_db', 'name=%s state=dump target=%s' % (self.db, self.dump_path)
            )

            feedback = Remote(dumpremote)
            feedback.show()

            logging.info("Fetching %s from %s" % (self.dump_path, self.host))
            getremote = self._exec(
                'fetch', 'src=%s dest=%s fail_on_missing=yes flat=yes validate_md5=yes' %
                (self.dump_path, self.dump_path)
            )

            feedback = Remote(getremote)
            feedback.show()

            self._remote_remove(self.dump_path)

    def put(self):
        if self.dump_path == '' or os.path.isfile(self.dump_path) is False:
            logging.critical("KO: there is no dumped file")
            sys.exit(1)

        logging.info("Importing %s to %s in %s" % (self.dump_path, self.db, self.host))
        if self.host == "localhost":
            retcode = subprocess.call("zcat %s | mysql %s" % (self.dump_path, self.db), shell=True)
            if retcode != 0:
                logging.critical("KO execution failed:", retcode)
                sys.exit(1)
            else:
                logging.info("OK: %s" % self.host)

        else:
            logging.info("Uploading the dumped file to %s" % self.host)
            uploaddata = self._exec(
                'copy', 'dest=%s force=yes src=%s mode=0644' % (self.dump_path, self.dump_path)
            )

            feedback = Remote(uploaddata)
            feedback.show()

            self._check_mysqldb()
            logging.info("Creating the database %s if it doesn't exists" % self.db)
            checkremote = self._exec('mysql_db', 'name=%s state=present' % self.db)

            feedback = Remote(checkremote)
            feedback.show()

            logging.info("Importing the new data to %s on %s" % (self.db, self.host))
            loadremote = self._exec(
                'mysql_db', 'name=%s state=import target=%s' % (self.db, self.dump_path)
            )

            feedback = Remote(loadremote)
            feedback.show()

            self._remote_remove(self.dump_path)

        # Remove the file in localhost when everything is finished
        self._local_remove(self.dump_path)

    def config(self, db, host="localhost", user="root"):
        self.db = db
        self.host = host
        self.user = user

        if self.host != "localhost":
            hosts = [self.host]
            self.inventory = ansible.inventory.Inventory(hosts)

    def _check_mysqldb(self):
        logging.info('Check if MySQLdb is available in remote host for ansible')
        mysqldb = self._exec('yum', 'name=MySQL-python state=present')

        feedback = Remote(mysqldb, 0)
        feedback.show()

    def _remote_remove(self, filename):
        logging.info('Cleaning remote %s from %s' % (filename, self.host))
        remote = self._exec('file', 'path=%s state=absent' % filename)

        feedback = Remote(remote, 0)
        feedback.show()

    def _exec(self, name, arguments):
        action = ansible.runner.Runner(
            module_name=name,
            module_args=arguments,
            pattern=self.host,
            remote_user=self.user,
            inventory=self.inventory,
            forks=10
        ).run()

        return action

    def _local_remove(self, filename):
        try:
            os.remove(filename)
        except OSError as e:
            if e.errno != errno.ENOENT:
                raise


@click.command()
@click.argument('database')
@click.option(
    '--source-host', prompt=True, help='The host from where the database dump is taken. Can be localhost'
)
@click.option(
    '--destination-host', prompt=True, help='The host to inject the database dump. Can be localhost'
)
@click.option('--user', default='root', help='The source user. Default: root')
@click.option(
    '--destination-database', default=None, help='Destination database. Default: source database'
)
@click.option('--destination-user', default=None, help='Destination user. Default:source user')
@click.option('--debug', is_flag=True, help='Activate debug mode')
def cli(database, source_host, destination_host, user, destination_database, destination_user, debug):
    """A $HOME/.my.cnf must be set for each user.\n
    MySQL-python must be installed in target hosts
    """
    if debug:
        log_level = 'debug'
    else:
        log_level = 'info'
    data_dump = Cpdb(database, source_host, user, log_level)

    logging.info("Dumping and getting %s from %s" % (database, source_host))
    data_dump.get()
    if not destination_database:
        destination_database = database
    if not destination_user:
        destination_user = user

    data_dump.config(destination_database, destination_host, destination_user)
    data_dump.put()
